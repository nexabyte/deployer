#!/usr/bin/env php
<?php

use Nexabyte\Deployer\Deployer;

require_once __DIR__.'/vendor/autoload.php';


$deployer = new Deployer();

$deployer->setKeepReleases(5);

$deployer->setDeployPath('/absolute/path/to/deployment/dir');

$deployer->setSharedDirs([
    'Data/Persistent',
    'Data/Logs',
    'Configuration/Production',
]);


//$deployer->setDbHost('host');
//$deployer->setDbTable('table');
//$deployer->setDbUser('user');
//$deployer->setDbPass('pass');

$deployer->addHook(Deployer::HOOK_SYMLINK_RELEASE_BEFORE, 'FLOW_CONTEXT=Production /usr/bin/php8.2 {{current_release_dir}}/flow flow:cache:flush');
$deployer->addHook(Deployer::HOOK_SYMLINK_RELEASE_BEFORE, 'FLOW_CONTEXT=Production /usr/bin/php8.2 {{current_release_dir}}/flow doctrine:migrate');
$deployer->addHook(Deployer::HOOK_SYMLINK_RELEASE_BEFORE, 'FLOW_CONTEXT=Production /usr/bin/php8.2 {{current_release_dir}}/flow resource:publish');


$deployer->deploy();
