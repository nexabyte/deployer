#!/usr/bin/env php
<?php

use Nexabyte\Deployer\Deployer;

require_once __DIR__.'/vendor/autoload.php';

$deployer = new Deployer();

$deployer->writeln("<info>Deployment started</info>");

$deployer->setKeepReleases(5);

$deployer->setDeployPath('/absolute/path/to/deployment/dir');

$deployer->setSharedDirs([
    'var',
    'storage/testdir',
]);

$deployer->setSharedFiles([
    'shared-file/shared.html',
    'test-shared-file',
]);

//$deployer->setDbHost('host');
//$deployer->setDbTable('table');
//$deployer->setDbUser('user');
//$deployer->setDbPass('pass');

$deployer->addHook(Deployer::HOOK_SYMLINK_RELEASE_BEFORE, 'echo {{current_release_dir}}; cd {{current_release_dir}}; ls -la');
$deployer->addHook(Deployer::HOOK_HANDLE_SHARED_BEFORE, 'echo {{current_release_dir}} && cd {{current_release_dir}} 2>&1 && ls -la');

//$deployer->addHook(Deployer::HOOK_SYMLINK_RELEASE, true, function ($deployer){
//    $deployer->writeln('hello world');
//});

$deployer->deploy();
