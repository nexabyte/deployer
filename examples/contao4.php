#!/usr/bin/env php
<?php

use Nexabyte\Deployer\Deployer;

require_once __DIR__.'/vendor/autoload.php';


$deployer = new Deployer();

$deployer->setKeepReleases(5);

$deployer->setDeployPath('/absolute/path/to/deployment/dir');

$deployer->setSharedDirs([
    'var/log',
    'files',
    'templates',
]);

$deployer->setSharedFiles([
    '.env.local',
    'system/config/localconfig.php',
]);

//$deployer->setDbUser('user');
//$deployer->setDbPass('pass');
//$deployer->setDbTable('table');

$deployer->addHook(Deployer::HOOK_HANDLE_SHARED_AFTER, 'cd {{current_release_dir}} && composer install --no-dev --optimize-autoloader --prefer-dist --no-interaction');

$deployer->addHook(Deployer::HOOK_SYMLINK_RELEASE_BEFORE, 'php {{current_release_dir}}/vendor/bin/contao-console cache:clear');
$deployer->addHook(Deployer::HOOK_SYMLINK_RELEASE_BEFORE, 'php {{current_release_dir}}/vendor/bin/contao-console contao:symlinks -n');
$deployer->addHook(Deployer::HOOK_SYMLINK_RELEASE_BEFORE, 'php {{current_release_dir}}/vendor/bin/contao-console contao:migrate -n');
$deployer->addHook(Deployer::HOOK_SYMLINK_RELEASE_BEFORE, 'php {{current_release_dir}}/vendor/bin/contao-console assets:install public --symlink --relative');


$deployer->deploy();
