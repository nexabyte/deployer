#!/usr/bin/env php
<?php

use Nexabyte\Deployer\Deployer;

require_once __DIR__.'/vendor/autoload.php';


$deployer = new Deployer();

$deployer->setKeepReleases(5);

$deployer->setDeployPath('/absolute/path/to/deployment/dir');

$deployer->setSharedDirs([
    'var/log',
    'var/sessions',
    'storage',
]);

$deployer->setSharedFiles([
    '.env.local',
]);

//$deployer->setDbHost('host');
//$deployer->setDbTable('table');
//$deployer->setDbUser('user');
//$deployer->setDbPass('pass');

$deployer->addHook(Deployer::HOOK_SYMLINK_RELEASE_BEFORE, '/usr/bin/php8.2 {{current_release_dir}}/bin/console cache:clear');
$deployer->addHook(Deployer::HOOK_SYMLINK_RELEASE_BEFORE, '/usr/bin/php8.2 {{current_release_dir}}/bin/console doctrine:migrations:migrate --no-interaction');
$deployer->addHook(Deployer::HOOK_SYMLINK_RELEASE_BEFORE, '/usr/bin/php8.2 {{current_release_dir}}/bin/console assets:install public --symlink --no-debug');


$deployer->deploy();
