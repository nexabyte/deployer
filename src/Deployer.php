<?php

namespace Nexabyte\Deployer;

use Exception;
use RuntimeException;
use ZipArchive;

class Deployer
{

    public const HOOK_UNZIP_RELEASE_BEFORE = 'unzip_release_before';
    public const HOOK_UNZIP_RELEASE_AFTER = 'unzip_release_after';
    public const HOOK_HANDLE_SHARED_BEFORE = 'handle_shared_before';
    public const HOOK_HANDLE_SHARED_AFTER = 'handle_shared_after';
    public const HOOK_DATABASE_BACKUP_BEFORE = 'database_backup_before';
    public const HOOK_DATABASE_BACKUP_AFTER = 'database_backup_after';
    public const HOOK_SYMLINK_RELEASE_BEFORE = 'symlink_release_before';
    public const HOOK_SYMLINK_RELEASE_AFTER = 'symlink_release_after';
    public const HOOK_CLEANUP_BEFORE = 'cleanup_before';
    public const HOOK_CLEANUP_AFTER = 'cleanup_after';

    protected ?int $keepReleases = 5;
    protected ?string $deployPath = null;
    /**
     * @var string[]
     */
    protected array $sharedDirs = [];
    /**
     * @var string[]
     */
    protected array $sharedFiles = [];
    protected bool $useRelativeSymlinks;
    protected string $dbUser;
    protected string $dbPass;
    protected string $dbTable;
    protected string $dbHost;
    protected bool $dbBackupCompress = true;

    protected string $releasesDir = 'releases';
    protected string $archiveDir = 'archive';
    protected string $mainDir = 'current';
    protected string $sharedDir = 'shared';
    protected string $uploadDir = 'upload';
    protected string $dbBackupDir = 'db_backup';

    protected ?array $releasesList = null;
    protected ?int $currentRelease = null;

    protected bool $omitUnzipRelease = false;

    /**
     * @var array<array{identifier: string, before: bool, command: string|callable}>
     */
    protected array $hooks = [];


    public function __construct()
    {
        $this->useRelativeSymlinks = $this->commandSupportsOption('ln', '--relative');
    }

    /**
     * @return int|null
     */
    public function getKeepReleases(): ?int
    {
        return $this->keepReleases;
    }

    /**
     * @param int|null $keepReleases
     */
    public function setKeepReleases(?int $keepReleases): void
    {
        $this->keepReleases = $keepReleases;
    }

    /**
     * @return string
     */
    public function getDeployPath(): string
    {
        // Set default to one folder above the script that runs the deployment
        if (null === $this->deployPath) {
            $runningScript = get_included_files()[0];
            $this->deployPath = dirname($runningScript, 2);
        }

        $path = rtrim($this->deployPath, '/ ');
        if (empty($path)) {
            return '';
        }

        return $path . '/';
    }


    /**
     * @param string $deployPath
     */
    public function setDeployPath(string $deployPath): void
    {
        $this->deployPath = $deployPath;
    }

    /**
     * @return array
     */
    public function getSharedDirs(): array
    {
        return $this->sharedDirs;
    }

    /**
     * @param array $sharedDirs
     */
    public function setSharedDirs(array $sharedDirs): void
    {
        $this->sharedDirs = $sharedDirs;
    }

    /**
     * @return array
     */
    public function getSharedFiles(): array
    {
        return $this->sharedFiles;
    }

    /**
     * @param array $sharedFiles
     */
    public function setSharedFiles(array $sharedFiles): void
    {
        $this->sharedFiles = $sharedFiles;
    }

    /**
     * @return bool
     * @deprecated Symlinks should always be relative
     */
    public function getUseRelativeSymlinks(): bool
    {
        return $this->useRelativeSymlinks;
    }

    /**
     * @param bool $useRelativeSymlinks
     * @deprecated Symlinks should always be relative
     */
    public function setUseRelativeSymlinks(bool $useRelativeSymlinks): void
    {
        $this->useRelativeSymlinks = $useRelativeSymlinks;
    }

    /**
     * @return bool
     */
    public function isOmitUnzipRelease(): bool
    {
        return $this->omitUnzipRelease;
    }

    /**
     * @param bool $omitUnzipRelease
     */
    public function setOmitUnzipRelease(bool $omitUnzipRelease): void
    {
        $this->omitUnzipRelease = $omitUnzipRelease;
    }

    /**
     * @return string
     */
    public function getDbUser(): string
    {
        return $this->dbUser;
    }

    /**
     * @param string $dbUser
     */
    public function setDbUser(string $dbUser): void
    {
        $this->dbUser = $dbUser;
    }

    /**
     * @return string
     */
    public function getDbPass(): string
    {
        return $this->dbPass;
    }

    /**
     * @param string $dbPass
     */
    public function setDbPass(string $dbPass): void
    {
        $this->dbPass = $dbPass;
    }

    /**
     * @return string
     */
    public function getDbTable(): string
    {
        return $this->dbTable;
    }

    /**
     * @param string $dbTable
     */
    public function setDbTable(string $dbTable): void
    {
        $this->dbTable = $dbTable;
    }

    /**
     * @return string
     */
    public function getDbHost(): string
    {
        return $this->dbHost;
    }

    /**
     * @param string $dbHost
     */
    public function setDbHost(string $dbHost): void
    {
        $this->dbHost = $dbHost;
    }


    /**
     * @return bool
     */
    public function isDbBackupCompress(): bool
    {
        return $this->dbBackupCompress;
    }

    /**
     * @param bool $dbBackupCompress
     */
    public function setDbBackupCompress(bool $dbBackupCompress): void
    {
        $this->dbBackupCompress = $dbBackupCompress;
    }


    /**
     * @param string $identifier
     * @param $command
     * @return void
     */
    public function addHook(string $identifier, $command): void
    {
        if (!is_callable($command) && !is_string($command)) {
            throw new RuntimeException('Command in hook is neither callable nor string.');
        }

        $this->hooks[] = [
            'identifier' => $identifier,
            'command' => $command,
        ];
    }

    /**
     * @return int
     */
    public function getCurrentRelease(): int
    {

        if (null !== $this->currentRelease) {
            return $this->currentRelease;
        }

        $newReleaseNumber = 1;
        $releases = $this->getReleasesList();
        if (count($releases)) {
            $lastRelease = end($releases);
            $newReleaseNumber = $lastRelease + 1;
        }

        $newBackupNumber = 1;
        $dbBackups = $this->getDbBackupList();
        if (count($dbBackups)) {
            $lastBackup = end($dbBackups);
            $newBackupNumber = $lastBackup + 1;
        }

        // Take the higher number
        $newReleaseNumber = max($newReleaseNumber, $newBackupNumber);

        // Cache result
        $this->currentRelease = $newReleaseNumber;

        return $this->currentRelease;
    }


    /**
     * @return void
     */
    public function unzipFilesInUploadDir(): void
    {

        // Gather all zip files in upload dir
        $folder = $this->getDeployPath().$this->uploadDir;
        $zipFiles = glob($folder.'/*.zip');
        if (!count($zipFiles)) {
            return;
        }

        if (!class_exists('ZipArchive')) {
            $this->writeln('<danger>Warning:</danger> ZipArchive is not installed. Zip files cannot be unpacked!');

            return;
        }

        // Unzip and unlink
        $zip = new ZipArchive;
        foreach ($zipFiles as $zipFile) {
            if (true === $zip->open($zipFile)) {
                $zip->extractTo($folder);
                $zip->close();
                unlink($zipFile);
            }
        }
    }


    /**
     * @return void
     * @throws ExecException
     */
    public function moveUploadDirToReleases(): void
    {

        $from = $this->getDeployPath() . $this->uploadDir;
        $to = $this->getDeployPath() . $this->releasesDir . '/' . $this->getCurrentRelease();


        if (!is_dir($from)) {
            throw new RuntimeException(sprintf('Directory "%s" is not present', $from));
        }

        // Check if uploadDir is empty
        if (2 === count(scandir($from))) {
            throw new RuntimeException(sprintf('Directory "%s" is empty', $from));
        }

        // Create releases dir
        $this->command(sprintf('mkdir -p %s', $this->getDeployPath() . $this->releasesDir));


        if (!@rename($from, $to)) {
            throw new RuntimeException(sprintf('Directory "%s" could not be moved', $from));
        }

        // Create fresh new upload dir
        $this->createUploadDir();
    }


    /**
     * @return void
     * @throws ExecException
     */
    public function handleShared(): void
    {

        // Validate shared_dir, find duplicates
        foreach ($this->sharedDirs as $a) {
            foreach ($this->sharedDirs as $b) {
                if ($a !== $b && strpos(rtrim($a, '/') . '/', rtrim($b, '/') . '/') === 0) {
                    throw new RuntimeException("Can not share same dirs `$a` and `$b`.");
                }
            }
        }


        // Handle dirs

        foreach ($this->sharedDirs as $dir) {

            $releaseDirPath = $this->getDeployPath() . $this->releasesDir . '/' . $this->getCurrentRelease() . '/' . $dir;

            $sharedDirPath = $this->getDeployPath() . $this->sharedDir . '/' . $dir;

            // Check if shared dir does not exist.
            if (!is_dir($sharedDirPath)) {
                // Create shared dir if it does not exist.
                $this->command(sprintf('mkdir -p %s', $sharedDirPath));

                // If release contains shared dir, copy that dir from release to shared.
                if (is_dir($releaseDirPath)) {
                    $this->command(sprintf('cp -rv %s %s', $releaseDirPath, dirname($sharedDirPath)));
                }
            }

            // Remove from source.
            $this->command(sprintf('rm -rf %s', $releaseDirPath));

            // Create path to shared dir in release dir if it does not exist.
            // Symlink will not create the path and will fail otherwise.
            $this->command(sprintf('mkdir -p %s', dirname($releaseDirPath)));

            // Symlink shared dir to release dir
            $this->createSymlink($sharedDirPath, $releaseDirPath);
        }


        // Handle files

        foreach ($this->getSharedFiles() as $file) {

            $dirname = dirname($file);
            $sharedDirPath = $this->getDeployPath() . $this->sharedDir . '/' . $dirname;
            $sharedFilePath = $this->getDeployPath() . $this->sharedDir . '/' . $file;
            $releaseDirPath = $this->getDeployPath() . $this->releasesDir . '/' . $this->getCurrentRelease() . '/' . $dirname;
            $releaseFilePath = $this->getDeployPath() . $this->releasesDir . '/' . $this->getCurrentRelease() . '/' . $file;

            // Create dir of shared file if not existing
            $this->command(sprintf('mkdir -p %s', $sharedDirPath));


            if (is_file($releaseFilePath)) {

                // Check if shared file does not exist in shared and file exist in release
                if (!is_file($sharedFilePath)) {
                    // Copy file in shared dir if not present
                    $this->command(sprintf('cp -rv %s %s', $releaseFilePath, $sharedFilePath));
                }

                // Remove from source.
                if (!unlink($releaseFilePath)) {
                    throw new RuntimeException(sprintf('File "%s" could not be deleted', $releaseFilePath));
                }
            }


            // Ensure dir is available in release
            $this->command(sprintf('mkdir -p %s', $releaseDirPath));


            // Touch shared
            $this->command(sprintf('touch %s', $sharedFilePath));

            // Symlink shared dir to release dir
            $this->createSymlink($sharedFilePath, $releaseFilePath);
        }


    }


    /**
     * Creating symlink from release to current
     *
     * @return void
     * @throws ExecException
     */
    public function symlinkRelease(): void
    {
        $releasePath = $this->getDeployPath() . $this->releasesDir . '/' . $this->getCurrentRelease();
        $finalPath = $this->getDeployPath() . $this->mainDir;

        $this->createSymlink($releasePath, $finalPath);
    }


    /**
     * Dumps the sql database
     *
     * @return void
     * @throws ExecException
     */
    public function dumpSql(): void
    {
        if (empty($this->dbTable)) {
            return;
        }

        $dbDir = $this->getDeployPath() . $this->dbBackupDir . '/' . $this->getCurrentRelease();
        $dbFile = $dbDir . '/db_backup.sql';

        // Create dir
        $this->command(sprintf('mkdir -p %s', $dbDir));


        $options = [
            '--single-transaction',
            '--opt',
            '--no-tablespaces',
            '--default-character-set=utf8',
            '--result-file=' . $dbFile,
        ];

        if (!empty($this->dbUser)) {
            $options[] = '-u "' . $this->dbUser . '"';
        }

        if (!empty($this->dbHost)) {
            $options[] = '-h "' . $this->dbHost . '"';
        }

        if ($this->dbPass) {
            $options[] = '-p"' . $this->dbPass . '"';
        }

        $command = 'mysqldump ' . implode(' ', $options) . ' ' . $this->dbTable;

        // Backup database
        $this->command($command);
        // mysqldump -u user -p"password" --single-transaction --quick --lock-tables=false --no-tablespaces --opt --skip-add-locks --default-character-set=utf8 --result-file=/location/for/dumpfile.sql dbname';


        if ($this->dbBackupCompress) {

            // Compress with zip
            $this->command(sprintf('zip -r %s %s', $dbFile . '.zip', $dbFile));
            // Delete file
            $this->command(sprintf('rm -f %s', $dbFile));
        }

    }


    /**
     * Cleanup releases
     *
     * @return void
     * @throws ExecException
     */
    public function cleanup(): void
    {

        // Move from releases dir to archive dir
        $releases = array_reverse($this->getReleasesList());
        foreach ($releases as $release) {
            if ($this->currentRelease === $release) {
                // Skip current release
                continue;
            }

            // Move to archive dir
            $dir = $this->getDeployPath().$this->releasesDir.'/'.$release;
            if (is_dir($dir)) {
                $newDir = $this->getDeployPath().$this->archiveDir.'/'.$release;
                if (!@rename($dir, $newDir)) {
                    throw new RuntimeException(sprintf('Directory "%s" could not be moved', $dir));
                }
            }
        }


        // Cleanup releases
        if (!$this->keepReleases || $this->keepReleases < 1) {
            // Keep unlimited releases.
            return;
        }

        // Delete releases from archive
        $releases = array_reverse($this->getArchiveList());
        $keep = $this->keepReleases - 1;
        while ($keep > 0) {
            array_shift($releases);
            --$keep;
        }
        $archivePath = $this->getDeployPath() . $this->archiveDir;
        foreach ($releases as $release) {
            $dir = $archivePath . '/' . $release;
            if (is_dir($dir)) {
                $this->command(sprintf('rm -rf %s', $dir));
            }
        }


        // Delete database backups
        $dbBackups = array_reverse($this->getDbBackupList());
        $keep = $this->keepReleases;
        while ($keep > 0) {
            array_shift($dbBackups);
            --$keep;
        }
        $backupPath = $this->getDeployPath() . $this->dbBackupDir;
        foreach ($dbBackups as $backup) {
            $dir = $backupPath . '/' . $backup;
            if (is_dir($dir)) {
                $this->command(sprintf('rm -rf %s', $dir));
            }
        }

    }


    /**
     * Deploy several stages
     */
    public function deploy(): void
    {
        error_reporting(0);

        try {

            // Create necessary dirs
            $this->createNecessaryDirs();


            if (!$this->omitUnzipRelease) {
                $this->runHooks(self::HOOK_UNZIP_RELEASE_BEFORE);
                $this->writeln('<info>Step:</info> Unzip files in upload folder');
                $this->unzipFilesInUploadDir();
                $this->runHooks(self::HOOK_UNZIP_RELEASE_AFTER);
            }


            $this->writeln('<info>Step:</info> Move upload dir to releases');
            $this->moveUploadDirToReleases();
            $this->writeln('Done');


            $this->runHooks(self::HOOK_HANDLE_SHARED_BEFORE);
            $this->writeln('<info>Step:</info> Handle shared resources');
            $this->handleShared();
            $this->writeln('Done');
            $this->runHooks(self::HOOK_HANDLE_SHARED_AFTER);


            if (!empty($this->dbTable)) {
                $this->runHooks(self::HOOK_DATABASE_BACKUP_BEFORE);
                $this->writeln('<info>Step:</info> Dump SQL');
                $this->dumpSql();
                $this->writeln('Done');
                $this->runHooks(self::HOOK_DATABASE_BACKUP_AFTER);
            }


            $this->runHooks(self::HOOK_SYMLINK_RELEASE_BEFORE);
            $this->writeln('<info>Step:</info> Symlink to current');
            $this->symlinkRelease();
            $this->writeln('Done');
            $this->runHooks(self::HOOK_SYMLINK_RELEASE_AFTER);


            $this->runHooks(self::HOOK_CLEANUP_BEFORE);
            $this->writeln('<info>Step:</info> Cleanup old releases');
            $this->cleanup();
            $this->writeln('Done');
            $this->runHooks(self::HOOK_CLEANUP_AFTER);

        } catch (Exception $e) {
            // Write a nice error message
            $this->writeln(sprintf('<danger>Deployment failed!</danger> Error: %s', $e->getMessage()));
            exit(1);
        }

    }

    /**
     * Runs hooks/commands
     *
     * @throws ExecException
     */
    public function runHooks(string $identifier): void
    {

        foreach ($this->hooks as $hook) {

            if (!isset($hook['identifier'], $hook['command'])) {
                continue;
            }

            if ($hook['identifier'] !== $identifier) {
                continue;
            }

            if (!is_string($hook['command']) && !is_callable($hook['command'])) {
                continue;
            }

            $command = $hook['command'];

            $this->writeln(sprintf('<info>Hook:</info> Run command "%s"', $hook['identifier']));

            if (is_string($command)) {
                // Replace vars
                $command = $this->replaceVars($command);
                $this->command($command);
            }

            if (is_callable($command)) {
                $command($this);
            }

        }
    }


    /**
     * Get a list of all release dirs
     *
     * @return int[]
     */
    public function getReleasesList(): array
    {
        return $this->getDirList($this->releasesDir);
    }


    /**
     * Get a list of all archive dirs
     *
     * @return int[]
     */
    public function getArchiveList(): array
    {
        return $this->getDirList($this->archiveDir);
    }



    /**
     * Get a list of all names inside a dir
     *
     * @return int[]
     */
    public function getDirList(string $dir): array
    {

        $path = $this->getDeployPath() . $dir;

        if (!is_dir($path)) {
            return [];
        }

        $list = [];
        foreach (scandir($path) as $tmpDir) {
            if (!is_dir($path . '/' . $tmpDir)) {
                continue;
            }
            if (in_array($tmpDir, ['.', '..'])) {
                continue;
            }
            if (!is_numeric($tmpDir)) {
                continue;
            }
            $list[] = (int)$tmpDir;
        }

        sort($list);

        return $list;
    }




    /**
     * Get a list of all db dirs
     *
     * @return int[]
     */
    public function getDbBackupList(): array
    {

        $dbDir = $this->getDeployPath() . $this->dbBackupDir;
        $dbDirList = [];

        if (!is_dir($dbDir)) {
            return $dbDirList;
        }

        foreach (scandir($dbDir) as $dir) {
            if (!is_dir($dbDir . '/' . $dir)) {
                continue;
            }
            if (in_array($dir, ['.', '..'])) {
                continue;
            }
            if (!is_numeric($dir)) {
                continue;
            }
            $dbDirList[] = (int)$dir;
        }

        sort($dbDirList);

        return $dbDirList;
    }


    /**
     * Creates the directory in that the built data can be transferred
     *
     * @return void
     * @throws ExecException
     */
    public function createUploadDir(): void
    {
        $this->createDir($this->uploadDir);
    }


    /**
     * @param string $dir
     *
     * @return void
     * @throws ExecException
     */
    public function createDir(string $dir): void
    {
        // Create new dir
        $this->command(sprintf('mkdir -p %s', $this->getDeployPath() . $dir));
    }


    /**
     * @return void
     * @throws ExecException
     */
    public function createNecessaryDirs(): void
    {
        // Create new dir
        $this->createDir($this->uploadDir);
        $this->createDir($this->archiveDir);
        $this->createDir($this->releasesDir);
    }


    /**
     * Writes a line to the stdout
     *
     * @param string $message
     * @return void
     */
    public function writeln(string $message): void
    {
        // Replace colors like <info>Infotext<info>
        $colors = ['info' => 34, 'warning' => 33, 'danger' => 31];
        $message = preg_replace_callback('/<(\/?)([a-z]+)>/ui', static function ($matches) use ($colors) {
            $code = $colors[$matches[2]] ?? null;
            if (!$code) {
                // Code not found. Return.
                return $matches[0];
            }
            // Check if it's a closing tag
            if ('/' === $matches[1]) {
                // Reset code to 'Default foreground color'
                $code = 39;
            }

            // Return color code
            return sprintf("\033[%sm", $code);
        }, $message);

        // Write to STDOUT
        fwrite(STDOUT, $message . PHP_EOL);
    }


    /**
     * Checks if a command is available on the *nix system
     *
     * @param string $command
     * @param string $option
     * @return bool
     */
    public function commandSupportsOption(string $command, string $option): bool
    {
        $test = sprintf("if [[ $(man %s 2>&1 || %s -h 2>&1 || %s --help 2>&1) =~ '%s' ]]; then echo 'true'; fi", $command, $command, $command, $option);
        $result = shell_exec($test);

        return 'true' === $result;
    }


    /**
     * Runs a command on the commandline
     *
     * @param string $string
     * @param bool $verbose
     * @return string[]
     * @throws ExecException
     */
    public function command(string $string, bool $verbose = true): array
    {

        exec($string, $output, $resultCode);
        if ($verbose) {
            foreach ($output as $line) {
                $this->writeln($line);
            }
        }
        if (0 !== $resultCode) {
            throw new ExecException(sprintf('Command failed: "%s"', $string));
        }

        return $output;
    }


    /**
     * Creates a symlink
     * Overrides previous ones
     *
     * @param string $target
     * @param string $linkName
     * @return void
     */
    protected function createSymlink(string $target, string $linkName): void
    {

        $targetArr = array_filter(explode('/', $target));
        $linkNameArr = array_filter(explode('/', $linkName));

        // Find same root folders
        $sameRootCount = 0;
        foreach ($targetArr as $key => $value) {
            if (isset($linkNameArr[$key]) && $linkNameArr[$key] === $value) {
                $sameRootCount++;
            } else {
                break;
            }
        }

        // Remove same root folders
        $targetArrRel = array_slice($targetArr, $sameRootCount);
        $linkNameArrRel = array_slice($linkNameArr, $sameRootCount);

        // Make link relative
        $relSubFolderCount = count($linkNameArrRel) - 1;
        $prepend = str_repeat('../', $relSubFolderCount);
        $targetRel = $prepend . implode('/', $targetArrRel);

        // Remove old link
        if (file_exists($linkName)) {
            unlink($linkName);
        }
        // Execute
        symlink($targetRel, $linkName);
    }


    protected function replaceVars(string $string): string
    {

        $deployPath = $this->getDeployPath();

        $replacements = [
            'deploy_path' => rtrim($deployPath, '/ '),
            'releases_dir' => $deployPath . $this->releasesDir,
            'main_dir' => $deployPath . $this->mainDir,
            'shared_dir' => $deployPath . $this->sharedDir,
            'upload_dir' => $deployPath . $this->uploadDir,
            'db_backup_dir' => $deployPath . $this->dbBackupDir,
            'current_release_dir' => $deployPath . $this->releasesDir . '/' . $this->getCurrentRelease(),
        ];

        $string = preg_replace_callback('/{{ ?([a-z_]+) ?}}/ui', static function ($matches) use ($replacements) {
            return $replacements[$matches[1]] ?? $matches[0];
        }, $string);

        return $string;
    }

}


class ExecException extends Exception
{
}
