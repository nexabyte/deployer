# Deployer

PHP script for handling deployed data on a server.

No data will be transferred to the server. This script is used with a CI/CD server, after the files have been built and transferred to the `upload` directory.

There will be a folder `releases` with the last releases and a symlink `current` with the currently active release created.

The PHP opcache has some problems with symlinks, so that the deprecated releases are moved to an `archive` folder.

If database credentials are given, the database will be dumped into a zip-file inside of the `db_backup` folder.



## How to

Use it in a file (i.e. `deploy.php`) like this:
```php
#!/usr/bin/env php
use Nexabyte\Deployer\Deployer;

include 'Deployer.php';

$deployer = new Deployer();

$deployer->writeln("<info>Deployment started</info>");
$deployer->setSharedDirs([
    'var',
    'storage',
]);
$deployer->setSharedFiles([
    '.env.local',
]);

// Make a DB backup (can be omitted)
$deployer->setDbUser('user');
$deployer->setDbPass('password');
$deployer->setDbTable('tablename');

// Deploy it
$deployer->deploy();
```


Then run it with:
```bash
./deploy.php
#or
php deploy.php
```



## Options

```php
// Change number of kept releases:
$deployer->setKeepReleases(5);

// Switch off zip compression for database backups:
$deployer->setDbBackupCompress(false);

// Write to the terminal:
$deployer->writeln("<info>Deployment started</info>");

// Change the deploy path
$deployer->setDeployPath('sub/folder');

// Add hooks
$deployer->addHook(Deployer::HOOK_HANDLE_SHARED_BEFORE, 'echo {{current_release_dir}} && cd {{current_release_dir}} 2>&1 && ls -la');

```
